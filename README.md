# soal-shift-sisop-modul-4-f11-2022

Anggota Kelompok

 - Arya Widia Putra     	5025201016
 - Muhammad Rolanov Wowor	5025201017
 - Muhammad Damas Abhirama	5025201271

## Soal 1
```
static  struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .access = xmp_access,
    .readlink = xmp_readlink,
    .readdir = xmp_readdir,
    .mknod = xmp_mknod,
    .mkdir = xmp_mkdir,
    .symlink = xmp_symlink,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
    .link = xmp_link,
    .chmod = xmp_chmod,
    .chown = xmp_chown,
    .truncate = xmp_truncate,
    .utimens = xmp_utimens,
    .open = xmp_open,
    .read = xmp_read,
    .write = xmp_write,
    .statfs = xmp_statfs,
    .create = xmp_create,
    // .release = xmp_release,
    // .fsync = xmp_fsync,
    // .setxattr = xmp_setxattr,
    // .getxattr = xmp_getxattr,
    // .listxattr = xmp_listxattr,
    // .removexattr = xmp_removexattr,
};
```
struktur di atas merupakan struktur fuse yang kami gunakan

1a. Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13

1b. Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.
```
void encryptAtbash(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	
	printf("encrypt path Atbash: %s\n", path);
	
	int endid = pisahkanExt(path);
	if(endid == strlen(path)) endid = extensionId(path);
	int startid = pemisahId(path, 0);
	char dest[1000];
	for (int i = startid; i < endid; i++){
		dest[i] = path[i];
		if (path[i] != '/' && isalpha(path[i])){
			if(path[i] >= 'A' && path[i] <= 'Z'){
				if(!((path[i]>=0&&path[i]<65)||(path[i]>90&&path[i]<97)||(path[i]>122&&path[i]<=127))){
		   			if(path[i]>='A'&&path[i]<='Z')
		   				path[i] = 'Z'+'A'-path[i];
		  			if(path[i]>='a'&&path[i]<='z')
		  				path[i] = 'z'+'a'-path[i];
	 			} 
			}
			
			else {
				if((path[i] >= 97 && path[i] <= 122) || (path[i] >= 65 && path[i] <= 90)){
		      		if(path[i] > 109 || (path[i] > 77 && path[i] < 91))
		        		path[i] -= 13;
		      		else
		        		path[i] += 13;
		    		}
      			}
		}
	}
	renamelog("RENAME terenkripsi", dest, path);
}
```
algoritma encryptAtbash untuk melakukan enkripsi dengan ketentuan file yang bernama "Animeku_" akan ter-enkripsi. setelah itu akan memanggil fungsi renamelog untuk me-record aktifitas di dalam wibu.log

1c. Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.
```
void decryptAtbash(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	
	printf("decrypt path Atbash: %s\n", path);
	
	int endid = pisahkanExt(path);
	if(endid == strlen(path)) endid = extensionId(path);
	int startid = pemisahId(path, endid);
	char dest[1000];
	
	for (int i = startid; i < endid; i++){
		dest[i] = path[i];
		if (path[i] != '/' && isalpha(path[i])){
			if(path[i] >= 'A' && path[i] <= 'Z'){
				if(!((path[i]>=0&&path[i]<65)||(path[i]>90&&path[i]<97)||(path[i]>122&&path[i]<=127))){
		   			if(path[i]>='A'&&path[i]<='Z')
		   				path[i] = 'Z'+'A'-path[i];
		  			if(path[i]>='a'&&path[i]<='z')
		  				path[i] = 'z'+'a'-path[i];
	 			} 
			}
			
			else {
				if((path[i] >= 97 && path[i] <= 122) || (path[i] >= 65 && path[i] <= 90)){
		      		if(path[i] > 109 || (path[i] > 77 && path[i] < 91))
		        		path[i] -= 13;
		      		else
		        		path[i] += 13;
		    		}
      			}
		}
	}
	renamelog("RENAME terdecode", dest, path);
}
```
algoritma decryptAtbash untuk melakukan decode file-file yang berada di folder yang telah di rename.

1d. Setiap data yang terencode akan masuk dalam file “Wibu.log”
```
void renamelog(const char *note, const char *from, const char *to)
{
	char haha[1000];

	FILE *file;
	file = fopen("/home/damas/wibu.log", "a");
	sprintf(haha, "%s %s/%s -> %s/%s\n",note, dirpath,from, dirpath,to);

	fputs(haha, file);
	fclose(file);
	return;
}
```
pada fungsi renamelog ini, program akan membuat file wibu.log pada directory tersebut dan melakukan append.

untuk meendapatkan index awal berupa slash (/) dan titik (.), kami menggunakan fungsi-fungsi berikut 
```
int pisahkanExt(char *path){ 
	int flag = 0;
	for(int i=strlen(path)-1; i>=0; i--){
		if (path[i] == '.'){
            if(flag == 1) return i;
            else flag = 1;
        }
	}
	return strlen(path);
}
```
fungsi pisahkanExt untuk mengembalikan index file extension pada file yang sudah dipisahkan. program akan melakukan looping sepanjang length nya. setelah menemukan titik (.) maka akan direturn kan posisi ke berapa titik tersebut.
```
int extensionId(char *path){ 
	for(int i=strlen(path)-1; i>=0; i--){
		if (path[i] == '.') return i;
	}
	return strlen(path);
}
```
fungsi extensionId untuk me-return index extension berada pada posisi ke berapa.
```
int pemisahId(char *path, int hasil){ 
	for(int i=0; i<strlen(path); i++){
		if (path[i] == '/') return i + 1;
	}
	return hasil;
}
```
fungsi pemisahId untuk mee-reeturn index slash beerada pada posisi ke berapa.

Hasil Run
 - Folder awal
<br>

![Gambar-folder-awal](/img/soal1_FolderAwal.png)

<br>

 - Setelah di encrypt
<br>

![Gambar-encrypt](/img/soal1_enkripsi.png)

<br>

 - Setelah di decrypt
<br>

![Gambar-decrypt](/img/soal1_dekripsi.png)

<br>

 - Wibu.log
 <br>

![Gambar-wibuLog](/img/soal1_wibuLog.png)

<br>

## Soal 2
2a. Membuat keperluan dan syarat untuk melakukan Vigenere Cypher, menentukan key "INNUGANTENG" dan variabel "IAN_"
```
char *key = "INNUGANTENG";
char *ian = "IAN_";
```
2b.
Algoritma Vigenere Ciphe, Encrypt untuk encode, Decrypt untuk decode
```
void encryptVigenere(char *path){
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	
	int idAkhir = pisahkanExt(path);
	int idAwal = pemisahId(path, 0);
	
	for (int i = idAwal; i < idAkhir; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char temp1 = path[i]; char temp2 = key[(i-idAwal) % strlen(key)];
			if(isupper(path[i])){temp1 -= 'A'; temp2 -= 'A'; temp1 = (temp1 + temp2) % 26; temp1 += 'A';}
			else{ temp1 -= 'a'; temp2 = tolower(temp2) - 'a'; temp1 = (temp1 + temp2) % 26; temp1 += 'a';}
			path[i] = temp1;
		}
	}
}

void decryptVigenere(char *path){
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	
	
	int idAkhir = pisahkanExt(path);
	int idAwal = pemisahId(path, idAkhir);
	
	for (int i = idAwal; i < idAkhir; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char temp1 = path[i];
			char temp2 = key[(i-idAwal) % strlen(key)];
			if(isupper(path[i])){temp1 -= 'A'; temp2 -= 'A';temp1 = (temp1 - temp2 + 26) % 26; temp1 += 'A';}
			else{temp1 -= 'a'; temp2 = tolower(temp2) - 'a'; temp1 = (temp1 - temp2 + 26) % 26; temp1 += 'a';}
			path[i] = temp1;
		}
	}
}
```

2d. Membuat Log System
```
void writeTheLog(char *nama, char *filepath)
{
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
 
	char infoWriteLog[1000];

	FILE *file;
	file = fopen("/home/damas/hayolongapain_F11.log", "a");

	if (strcmp(nama, "RMDIR") == 0 || strcmp(nama, "UNLINK") == 0)
		sprintf(infoWriteLog, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, filepath);
	else
		sprintf(infoWriteLog, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, filepath);

	fputs(infoWriteLog, file);
	fclose(file);
	return;
}

void writeTheLog2(char *nama, const char *from, const char *to)
{
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);

	char infoWriteLog[1000];

	FILE *file;
	file = fopen("/home/ianfelix/hayolongapain_F11.log", "a");

	if (strcmp(nama, "RMDIR") == 0 || strcmp(nama, "UNLINK") == 0)
		sprintf(infoWriteLog, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, from, to);
	else
		sprintf(infoWriteLog, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, from, to);

	fputs(infoWriteLog, file);
	fclose(file);
	return;
}

```
Fungsi WriteTheLog untuk aktivitas yang tidak terjadi perpindahan direktori, dan fungsi writeTheLog2 untuk aktivitas yang terjadi perpindahan direktori.

2e. Sistem level log
```
if (strcmp(nama, "RMDIR") == 0 || strcmp(nama, "UNLINK") == 0)
		sprintf(infoWriteLog, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, filepath);
	else
		sprintf(infoWriteLog, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, filepath);
```

Memberikan level WARNING untuk syscall RMDIR dan UNLINK, dan level INFO untuk syscall lain
