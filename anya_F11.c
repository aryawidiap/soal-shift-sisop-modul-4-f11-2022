#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

char *key = "INNUGANTENG";
char *ian = "IAN_";
char *anime = "Animeku_";

int x = 0;

static const char *dirpath = "/home/damas/Documents";
char Huruf_besar[] = {'A', 'B', 'C', 'D', 'E', 'F',
                      'G', 'H', 'I', 'J', 'K', 'L',
                      'M', 'N', 'O', 'P', 'Q', 'R',
                      'S', 'T', 'U', 'V', 'W', 'X',
                      'Y', 'Z'};
char Huruf_kecil[] = {'a', 'b', 'c', 'd', 'e', 'f',
                      'g', 'h', 'i', 'j', 'k', 'l',
                      'm', 'n', 'o', 'p', 'q', 'r',
                      's', 't', 'u', 'v', 'w', 'x',
                      'y', 'z'};

int pisahkanExt(char *path){ //split ext id
	int flag = 0;
	for(int i=strlen(path)-1; i>=0; i--){
		if (path[i] == '.'){
            if(flag == 1) return i;
            else flag = 1;
        }
	}
	return strlen(path);
}

int extensionId(char *path){ //ext id
	for(int i=strlen(path)-1; i>=0; i--){
		if (path[i] == '.') return i;
	}
	return strlen(path);
}

int pemisahId(char *path, int hasil){ //slash id
	for(int i=0; i<strlen(path); i++){
		if (path[i] == '/') return i + 1;
	}
	return hasil;
}

void encryptAtbash(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	
	printf("encrypt path Atbash: %s\n", path);
	
	int endid = pisahkanExt(path);
	if(endid == strlen(path)) endid = extensionId(path);
	int startid = pemisahId(path, 0);
	char dest[1000];
	for (int i = startid; i < endid; i++){
		dest[i] = path[i];
		if (path[i] != '/' && isalpha(path[i])){
			if(path[i] >= 'A' && path[i] <= 'Z'){
				if(!((path[i]>=0&&path[i]<65)||(path[i]>90&&path[i]<97)||(path[i]>122&&path[i]<=127))){
		   			if(path[i]>='A'&&path[i]<='Z')
		   				path[i] = 'Z'+'A'-path[i];
		  			if(path[i]>='a'&&path[i]<='z')
		  				path[i] = 'z'+'a'-path[i];
	 			} 
			}
			
			else {
				if((path[i] >= 97 && path[i] <= 122) || (path[i] >= 65 && path[i] <= 90)){
		      		if(path[i] > 109 || (path[i] > 77 && path[i] < 91))
		        		path[i] -= 13;
		      		else
		        		path[i] += 13;
		    		}
      			}
		}
	}
	renamelog("RENAME terenkripsi", dest, path);
}

void decryptAtbash(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	
	printf("decrypt path Atbash: %s\n", path);
	
	int endid = pisahkanExt(path);
	if(endid == strlen(path)) endid = extensionId(path);
	int startid = pemisahId(path, endid);
	char dest[1000];
	
	for (int i = startid; i < endid; i++){
		dest[i] = path[i];
		if (path[i] != '/' && isalpha(path[i])){
			if(path[i] >= 'A' && path[i] <= 'Z'){
				if(!((path[i]>=0&&path[i]<65)||(path[i]>90&&path[i]<97)||(path[i]>122&&path[i]<=127))){
		   			if(path[i]>='A'&&path[i]<='Z')
		   				path[i] = 'Z'+'A'-path[i];
		  			if(path[i]>='a'&&path[i]<='z')
		  				path[i] = 'z'+'a'-path[i];
	 			} 
			}
			
			else {
				if((path[i] >= 97 && path[i] <= 122) || (path[i] >= 65 && path[i] <= 90)){
		      		if(path[i] > 109 || (path[i] > 77 && path[i] < 91))
		        		path[i] -= 13;
		      		else
		        		path[i] += 13;
		    		}
      			}
		}
	}
	renamelog("RENAME terdecode", dest, path);
}

void encryptVigenere(char *path){
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	
	int idAkhir = pisahkanExt(path);
	int idAwal = pemisahId(path, 0);
	
	for (int i = idAwal; i < idAkhir; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char temp1 = path[i]; char temp2 = key[(i-idAwal) % strlen(key)];
			if(isupper(path[i])){temp1 -= 'A'; temp2 -= 'A'; temp1 = (temp1 + temp2) % 26; temp1 += 'A';}
			else{ temp1 -= 'a'; temp2 = tolower(temp2) - 'a'; temp1 = (temp1 + temp2) % 26; temp1 += 'a';}
			path[i] = temp1;
		}
	}
}

void decryptVigenere(char *path){
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	int idAkhir = pisahkanExt(path);
	int idAwal = pemisahId(path, idAkhir);
	
	for (int i = idAwal; i < idAkhir; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char temp1 = path[i];
			char temp2 = key[(i-idAwal) % strlen(key)];
			if(isupper(path[i])){temp1 -= 'A'; temp2 -= 'A';temp1 = (temp1 - temp2 + 26) % 26; temp1 += 'A';}
			else{temp1 -= 'a'; temp2 = tolower(temp2) - 'a'; temp1 = (temp1 - temp2 + 26) % 26; temp1 += 'a';}
			path[i] = temp1;
		}
	}
}

void enkripsiMenjadiRot13(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	
	
	int idAkhir = pisahkanExt(path);
	int idAwal = pemisahId(path, 0);
	
	for (int i = idAwal; i < idAkhir; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char sementara = path[i];
			if(isupper(path[i])) sementara -= 'A'; else sementara -= 'a'; sementara = (sementara + 13) % 26;
			if(isupper(path[i])) sementara += 'A'; else sementara += 'a';
			path[i] = sementara;
		}
	}
}

void dekripsiMenjadiRot13(char *path)
{
	if (!strcmp(path, ".") || !strcmp(path, "..")) return;
	
	
	int idAkhir = pisahkanExt(path);
	int idAwal = pemisahId(path, idAkhir);
	
	for (int i = idAwal; i < idAkhir; i++){
		if (path[i] != '/' && isalpha(path[i])){
			char sementara = path[i];
			if(isupper(path[i])) sementara -= 'A'; else sementara -= 'a'; sementara = (sementara + 13) % 26;
			if(isupper(path[i])) sementara += 'A'; else sementara += 'a';
			path[i] = sementara;
		}
	}
}

void renamelog(const char *note, const char *from, const char *to)
{
	char haha[1000];

	FILE *file;
	file = fopen("/home/damas/wibu.log", "a");
	sprintf(haha, "%s %s/%s -> %s/%s\n",note, dirpath,from, dirpath,to);

	fputs(haha, file);
	fclose(file);
	return;
}


void writeTheLog(char *nama, char *filepath)
{
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
 
	char infoWriteLog[1000];

	FILE *file;
	file = fopen("/home/damas/hayolongapain_F11.log", "a");

	if (strcmp(nama, "RMDIR") == 0 || strcmp(nama, "UNLINK") == 0)
		sprintf(infoWriteLog, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, filepath);
	else
		sprintf(infoWriteLog, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, filepath);

	fputs(infoWriteLog, file);
	fclose(file);
	return;
}

void writeTheLog2(char *nama, const char *from, const char *to)
{
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);

	char infoWriteLog[1000];

	FILE *file;
	file = fopen("/home/damas/hayolongapain_F11.log", "a");

	if (strcmp(nama, "RMDIR") == 0 || strcmp(nama, "UNLINK") == 0)
		sprintf(infoWriteLog, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, from, to);
	else
		sprintf(infoWriteLog, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, from, to);

	fputs(infoWriteLog, file);
	fclose(file);
	return;
}

static int xmp_unlink(const char *path)
{
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){
        path = dirpath;
        sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	result = unlink(filepath);
	writeTheLog("UNLINK", filepath);

	if (result == -1) return -errno;
	return 0;
}

static int xmp_getattr(const char *path, struct stat *stbuf){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);
	

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	result = lstat(filepath, stbuf);
	if (result == -1) return -errno;
	return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	if (x != 24) x++;
	else writeTheLog("READDIR", filepath);

	int result = 0;
	DIR *dp;
	struct dirent *de;

	(void)offset;
	(void)fi;

	dp = opendir(filepath);
	if (dp == NULL) return -errno;

	while ((de = readdir(dp)) != NULL){
		if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) continue;

		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;

		if (a != NULL) encryptAtbash(de->d_name);
		if (b != NULL) encryptAtbash(de->d_name);

		result = (filler(buf, de->d_name, &st, 0));
		if (result != 0) break;
	}

	closedir(dp);
	return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){path = dirpath; sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	int result = 0;
	int fd = 0;

	(void)fi;
	writeTheLog("READ", filepath);

	fd = open(filepath, O_RDONLY);
	if (fd == -1) return -errno;

	result = pread(fd, buf, size, offset);
	if (result == -1) result = -errno;

	close(fd);
	return result;
}

static int xmp_mkdir(const char *path, mode_t mode){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL){
		decryptAtbash(a);
	}

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	result = mkdir(filepath, mode);
	writeTheLog("MKDIR", filepath);

	if (result == -1) return -errno;
	return 0;
}

static int xmp_rmdir(const char *path)
{
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b= strstr(path, ian);
	if (b!= NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	result = rmdir(filepath);
	writeTheLog("RMDIR", filepath);

	if (result == -1) return -errno;
	return 0;
}

static int xmp_rename(const char *from, const char *to){
	int result;
	char dariDir[1000], keDir[1000];
	
	char *a =strstr(to, anime);
	if (a != NULL) {
		decryptAtbash(a);
	}

	char *b = strstr(to, ian);
    if (b != NULL) {
        decryptVigenere(b);
    }

	sprintf(dariDir, "%s%s", dirpath, from);
	sprintf(keDir, "%s%s", dirpath, to);

	result = rename(dariDir, keDir);
	if (result == -1) return -errno;

	writeTheLog2("RENAME", dariDir, keDir);

	return 0;
}

static int xmp_truncate(const char *path, off_t size){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	writeTheLog("TRUNC", filepath);
	result = truncate(filepath, size);
	
	if (result == -1) return -errno;
	return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
	int fd;
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	(void)fi;
	writeTheLog("WRITE", filepath);

	fd = open(filepath, O_WRONLY);
	if (fd == -1) return -errno;

	result = pwrite(fd, buf, size, offset);
	if (result == -1) result = -errno;

	close(fd);
	return result;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	(void)fi;
	writeTheLog("CREATE", filepath);
	
	result = creat(filepath, mode);	
	if (result == -1) return -errno;

	close(result);
	return 0;
}

static int xmp_utimens(const char *path, const struct timespec ts[2])
{
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	struct timeval tv[2];

	tv[0].tv_sec = ts[0].tv_sec;
	tv[0].tv_usec = ts[0].tv_nsec / 1000;
	tv[1].tv_sec = ts[1].tv_sec;
	tv[1].tv_usec = ts[1].tv_nsec / 1000;

	result = utimes(filepath, tv);
	if (result == -1) return -errno;
	return 0;
}

static int xmp_access(const char *path, int mask){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, ian);
	if (a != NULL) decryptVigenere(a);

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	result = access(filepath, mask);
	if (result == -1) return -errno;
	return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	result = open(filepath, fi->flags);

	writeTheLog("OPEN", filepath);
	if (result == -1) return -errno;

	close(result);
	return 0;
}

static int xmp_readlink(const char *path, char *buf, size_t size)
{
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	result = readlink(filepath, buf, size - 1);
	writeTheLog("READLINK", filepath);
	if (result == -1) return -errno;

	buf[result] = '\0';
	return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	writeTheLog("MKNOD", filepath);
	
	if (S_ISREG(mode)){
		result = open(filepath, O_CREAT | O_EXCL | O_WRONLY, mode);
		if (result >= 0) result = close(result);
	}else if (S_ISFIFO(mode)) result = mkfifo(filepath, mode);
	else result = mknod(filepath, mode, rdev);
	
	if (result == -1) return -errno;
	return 0;
}

static int xmp_symlink(const char *from, const char *to){
	int result;
	char dariDir[1000], keDir[1000];
	
	char *a = strstr(to, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(to, ian);
	if (b != NULL) decryptVigenere(b);

	sprintf(dariDir, "%s%s", dirpath, from);
	sprintf(keDir, "%s%s", dirpath, to);

	result = symlink(dariDir, keDir);
	writeTheLog2("SYMLINK", dariDir, keDir);
	
	if (result == -1) return -errno;
	return 0;
}

static int xmp_link(const char *from, const char *to)
{
	int result;
	char dariDir[1000], keDir[1000];
	
	char *a = strstr(from, anime);
	if (a != NULL){
        decryptAtbash(a);
    }

	char *b = strstr(from, ian);
	if (b != NULL){
        decryptVigenere(b);
    }


	sprintf(dariDir, "%s%s", dirpath, from);
	sprintf(keDir, "%s%s", dirpath, to);

	result = link(dariDir, keDir);
	writeTheLog2("LINK", dariDir, keDir);

	if (result == -1) return -errno;
	return 0;
}

static int xmp_chmod(const char *path, mode_t mode){
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);
	
	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	result = chmod(filepath, mode);
	writeTheLog("CHMOD", filepath);

	if (result == -1) return -errno;
	return 0;
}

static int xmp_chown(const char *path, uid_t uid, gid_t gid)
{
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	result = lchown(filepath, uid, gid);
	writeTheLog("CHOWN", filepath);
	
	if (result == -1) return -errno;
	return 0;
}

static int xmp_statfs(const char *path, struct statvfs *stbuf)
{
	int result;
	char filepath[1000];
	
	char *a = strstr(path, anime);
	if (a != NULL) decryptAtbash(a);

	char *b = strstr(path, ian);
	if (b != NULL) decryptVigenere(b);

	if (strcmp(path, "/") == 0){path = dirpath;sprintf(filepath, "%s", path);}
	else{sprintf(filepath, "%s%s", dirpath, path);}

	result = statvfs(filepath, stbuf);
	writeTheLog("STATFS", filepath);
	
	if (result == -1) return -errno;
	return 0;
}

static  struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .access = xmp_access,
    .readlink = xmp_readlink,
    .readdir = xmp_readdir,
    .mknod = xmp_mknod,
    .mkdir = xmp_mkdir,
    .symlink = xmp_symlink,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
    .link = xmp_link,
    .chmod = xmp_chmod,
    .chown = xmp_chown,
    .truncate = xmp_truncate,
    .utimens = xmp_utimens,
    .open = xmp_open,
    .read = xmp_read,
    .write = xmp_write,
    .statfs = xmp_statfs,
    .create = xmp_create,
    // .release = xmp_release,
    // .fsync = xmp_fsync,
    // .setxattr = xmp_setxattr,
    // .getxattr = xmp_getxattr,
    // .listxattr = xmp_listxattr,
    // .removexattr = xmp_removexattr,
};

int main(int argc, char *argv[])
{
	umask(0);
	return fuse_main(argc, argv, &xmp_oper, NULL);
}


